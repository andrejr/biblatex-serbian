#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import re
import sys
from functools import reduce
from itertools import dropwhile
from itertools import takewhile
from textwrap import dedent
from typing import Dict
from typing import Iterable
from typing import List
from typing import Pattern
from typing import TextIO
from typing import Tuple
from typing import Union

import yaml

BibStrEntryDict = Dict[str, Union[str, List[str]]]

_regex_multiline_wrap = re.compile(r"\%.*?\n\s*", re.DOTALL)


def wrap_multiline_tex(tex: str) -> str:
    return _regex_multiline_wrap.sub("", tex)


bib_line_regex = re.compile(
    r"\s*(?P<key>\S+)\s*?=\s*?{\s*?{(?P<long>.+?)}\s*?{(?P<short>.+?)}}"
)

_macro_to_interp_whitesp_rx_repl_pairs = [
    (re.compile(r"\\adddot(?:\\)? "), ". "),
    (re.compile(r"\\adddot *$"), "."),
    (re.compile(r"\\addspace "), " "),
    (re.compile(r"\\addnbspace "), "~"),
    (re.compile(r"\\addabbrvspace "), "`"),
    (re.compile(r"\\adddotspace "), ".`"),
    (re.compile(r"\\addslash "), "/"),
    (re.compile(r"\\addcolon"), ":"),
]


def macro_to_interp_whitesp(text: str) -> str:
    def _apply_rx_sub(text: str, rx_sub: Tuple[Pattern[str], str]) -> str:
        rx, sub = rx_sub
        return rx.sub(sub, text)

    return reduce(_apply_rx_sub, _macro_to_interp_whitesp_rx_repl_pairs, text)


def merge_dicts(dicts: Iterable[dict]) -> dict:
    final_dict = {}
    for dct in dicts:
        final_dict.update(dct)
    return final_dict


def parse_bib_line(line: str) -> BibStrEntryDict:
    match = bib_line_regex.match(line)
    if match:
        key, long, short = match.groups()
        if long == short:
            return {key: long}
        else:
            return {key: [long, short]}
    raise AttributeError(f"Unmatched line ({line}).")


def extract_localization_strings(lbx_file: TextIO) -> BibStrEntryDict:
    lines_after_bibstr_begin = dropwhile(
        lambda l: not l.startswith(r"\DeclareBibliographyStrings"), lbx_file
    )
    # omit the \DeclareBibliographyStrings line
    next(lines_after_bibstr_begin)

    bibstr_lines = takewhile(
        lambda l: not l.startswith(r"}"), lines_after_bibstr_begin
    )

    bibstr = "".join(bibstr_lines)

    wrapped_bibstr = wrap_multiline_tex(bibstr)

    bib_entries = merge_dicts(
        parse_bib_line(line) for line in wrapped_bibstr.splitlines()
    )

    return bib_entries


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description=dedent(
            """\
        Bibliography localization string (.lbx) extractor.

        Extracts localization strings from (.lbx) files."""
        )
    )

    parser.add_argument(
        "infile",
        nargs="?",
        type=argparse.FileType("r", encoding="utf-8"),
        default=sys.stdin,
        help="Input file. If omitted, reads STDIN.",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        type=argparse.FileType("w", encoding="utf-8"),
        default=sys.stdout,
        help="Output file. If omitted, writes to STDOUT.",
    )

    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    with args.infile as infile, args.outfile as outfile:
        bib_entries = extract_localization_strings(infile)
        yaml.dump(bib_entries, outfile, allow_unicode=True)


if __name__ == "__main__":
    main()
