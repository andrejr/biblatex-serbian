# vim:set et sw=4 ts=4 tw=79 ft=perl:

# use lualatex
$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;

$lualatex = "lualatex -interaction nonstopmode -halt-on-error -file-line-error %O %P";

# use xelatex
# $pdf_mode = 5;
# $postscript_mode = $dvi_mode = 0;

# process index
$makeindex = 'makeindex -s ginddt2s.ist %O -o %D %S';

# process glossary
add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
  system("makeindex -q -s gglodt2s.ist -o '$_[0].gls' '$_[0].glo'");
}

# additional files to clean.
$clean_ext = 'bbl glo gls hd loa run.xml thm xdv ind idx';
