#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import yaml

from extract_localization_strings import extract_localization_strings


def main():
    with open(
        "serbian_latin_loc_strings.yaml", "r", encoding="utf-8"
    ) as srfile, open("english.lbx", "r", encoding="utf-8") as engfile:
        serbian_str = yaml.load(srfile, Loader=yaml.FullLoader)
        eng_str = extract_localization_strings(engfile)

        missing_in_serbian = {
            key: eng_str[key] for key in (eng_str.keys() - serbian_str.keys())
        }

    with open("missing_in_serbian.yaml", "w", encoding="utf-8") as sr_miss:
        yaml.dump(missing_in_serbian, sr_miss)


if __name__ == "__main__":
    main()
